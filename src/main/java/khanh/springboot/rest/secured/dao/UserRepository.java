package khanh.springboot.rest.secured.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import khanh.springboot.rest.secured.dao.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public List<User> findAll();
	public User getById(Integer id);
}
