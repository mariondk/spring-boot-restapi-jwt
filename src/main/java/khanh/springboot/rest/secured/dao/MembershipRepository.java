package khanh.springboot.rest.secured.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import khanh.springboot.rest.secured.dao.entity.Membership;

public interface MembershipRepository extends JpaRepository<Membership, Integer> {

	public List<Membership> findAll();
	public Membership findByUserId(int userId);
}
