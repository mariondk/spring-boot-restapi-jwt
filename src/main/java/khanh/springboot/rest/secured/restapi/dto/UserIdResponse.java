package khanh.springboot.rest.secured.restapi.dto;

import java.util.List;

public class UserIdResponse extends Response {

	public List<Integer> ids;

	public UserIdResponse(boolean success, String message, List<Integer> ids) {
		super(success, message);
		this.ids = ids;
	}
}
