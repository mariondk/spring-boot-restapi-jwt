package khanh.springboot.rest.secured.restapi.dto;

public class Response {

	public static final boolean SUCCESS = true;
	public static final boolean FAIL = false;
	
	public boolean success;
	public String message;

	public Response(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}
}
