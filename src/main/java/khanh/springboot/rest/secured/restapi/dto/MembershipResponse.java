package khanh.springboot.rest.secured.restapi.dto;

public class MembershipResponse extends Response {

	public MembershipDto membership;

	public MembershipResponse(boolean success, String message, MembershipDto membership) {
		super(success, message);
		this.membership = membership;
	}
}
