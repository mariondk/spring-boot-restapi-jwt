package khanh.springboot.rest.secured.restapi;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import khanh.springboot.rest.secured.dao.MembershipRepository;
import khanh.springboot.rest.secured.dao.UserRepository;
import khanh.springboot.rest.secured.dao.entity.Membership;
import khanh.springboot.rest.secured.dao.entity.User;
import khanh.springboot.rest.secured.restapi.dto.MembershipDto;
import khanh.springboot.rest.secured.restapi.dto.MembershipResponse;
import khanh.springboot.rest.secured.restapi.dto.MembershipStartDateResponse;
import khanh.springboot.rest.secured.restapi.dto.Response;
import khanh.springboot.rest.secured.restapi.dto.UserDto;
import khanh.springboot.rest.secured.restapi.dto.UserIdResponse;
import khanh.springboot.rest.secured.restapi.dto.UserNameResponse;
import khanh.springboot.rest.secured.restapi.dto.UserResponse;



@RestController
@RequestMapping(value = UserController.URI.API)
public class UserController {

	public static final class URI {

		public static final String API = "/api/user";

		public static final String RESOURCE_NAME = "/name";
		public static final String MEMBERSHIP = "/membership";
		public static final String MEMBERSHIP_DATE = "/membership_date";
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserRepository userrepository;
	@Autowired
	private MembershipRepository membershipRepository;
	
	@RequestMapping(method = RequestMethod.GET, value = URI.RESOURCE_NAME)
	public UserNameResponse getUserNames(@RequestParam(required = true, name = "userid") Integer userId) {
		LOGGER.info("Request User Name");
		User jpas;
		jpas = userrepository.getById(userId);
		return new UserNameResponse(Response.SUCCESS, "success", jpas.getName());
	}
	
	@RequestMapping(method = RequestMethod.GET, value = URI.MEMBERSHIP_DATE)
	public MembershipStartDateResponse getMembershipStartDate(@RequestParam(required = true, name = "userid") Integer userId) {
		LOGGER.info("Request Membership start date");
		Membership jpas;
		jpas = membershipRepository.findByUserId(userId);
		Date start_date = jpas.getStart_date();
		return new MembershipStartDateResponse(Response.SUCCESS, "success", start_date);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = URI.MEMBERSHIP)
	public MembershipResponse getMembership(@RequestParam(required = false, name = "userid") Integer userId) {
		LOGGER.info("Request User");
		Membership jpas;
		jpas = membershipRepository.findByUserId(userId);
		long diff = new Date().getTime()- jpas.getStart_date().getTime();
		int days = (int)(diff / (1000 * 60 * 60 * 24))  ;
		int loyal = (days + 1) * 5;
		
		MembershipDto dtos = new MembershipDto(jpas.getUser().getId(), jpas.getUser().getName(), jpas.getStart_date(), loyal);
		return new MembershipResponse(Response.SUCCESS, "success", dtos);
	}
}
