package khanh.springboot.rest.secured.restapi.dto;

public class UserDto {

	public int id;
	public String name;

	public UserDto(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
}
