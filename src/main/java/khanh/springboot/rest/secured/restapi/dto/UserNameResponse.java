package khanh.springboot.rest.secured.restapi.dto;


public class UserNameResponse extends Response {

	public String name;

	public UserNameResponse(boolean success, String message, String name) {
		super(success, message);
		this.name = name;
	}
}
