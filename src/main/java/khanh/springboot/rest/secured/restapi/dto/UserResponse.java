package khanh.springboot.rest.secured.restapi.dto;

import java.util.List;


public class UserResponse extends Response {

	public List<UserDto> users;

	public UserResponse(boolean success, String message, List<UserDto> users) {
		super(success, message);
		this.users = users;
	}
}
