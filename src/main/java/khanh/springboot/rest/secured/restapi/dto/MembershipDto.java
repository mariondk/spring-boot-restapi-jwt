package khanh.springboot.rest.secured.restapi.dto;

import java.util.Date;

public class MembershipDto {
	public int id;
	public String name;
	public Date start_date;
	public int loyal_point;
	
	public MembershipDto(int id, String name, Date start_date, int loyal_point) {
		super();
		this.id = id;
		this.name = name;
		this.start_date = start_date;
		this.loyal_point = loyal_point;
	}

}
