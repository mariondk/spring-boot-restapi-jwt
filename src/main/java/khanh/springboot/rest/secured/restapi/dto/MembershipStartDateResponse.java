package khanh.springboot.rest.secured.restapi.dto;

import java.util.Date;

public class MembershipStartDateResponse extends Response{

	public Date start_date;

	public MembershipStartDateResponse(boolean success, String message, Date start_date) {
		super(success, message);
		this.start_date = start_date;
	}
}
